import cwiid
import time

print 'Press 1+2 on your Wiimote now...'
wm = cwiid.Wiimote()

time.sleep(1)

wm.rpt_mode = cwiid.RPT_BTN | cwiid.RPT_ACC

wm.led = 1

while True:
    wm.rumble = (wm.state['acc'][0] < 126)
    if wm.state['buttons'] & cwiid.BTN_A:
        wm.led = (wm.state['led'] + 1) % 16

    if wm.state['buttons'] == 4096:
		print 'closing Bluetooth connection. Good Bye!'
		time.sleep(1)
		exit(wm)
    time.sleep(.2)