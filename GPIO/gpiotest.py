import RPi.GPIO as GPIO
import time

def main():
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(18, GPIO.OUT)
	GPIO.setup(23, GPIO.IN)
	GPIO.setwarnings(False)
	GPIO.output(18, GPIO.LOW)
	Continue = True
	
	while Continue:
		buttonPress = GPIO.input(23)
		print "BUTTON PRESSED?: ", buttonPress
		if buttonPress == True:
			GPIO.output(18, GPIO.HIGH)
			print "LED: HIGH"
			time.sleep(2)
			GPIO.output(18, GPIO.LOW)
			print "LED: LOW"
			time.sleep(2)
			Continue = False
	print "PROGRAM TERMINATING"
	GPIO.cleanup()

if __name__ == '__main__':
	main()