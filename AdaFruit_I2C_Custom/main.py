import sys
import time
from I2C_DAC import I2C_DAC

dac = I2C_DAC(0x60, True)

def Bright(min, max):
	if (min > max):
		dac.setVolt(0)
		print "Error detected.  Minimum voltage exceeds maximum."
		#time.sleep(3)
	else:
		voltage = min
		for voltage in range (min, max, 10):
			dac.setVolt(voltage)
			#time.sleep(.05)


def Dim(max, min):
	if (min > max):
		dac.setVolt(0)
		print "Error detected.  Minimum voltage exceeds maximum."
		#time.sleep(3)
	else:
		voltage = max
		for voltage in range (max,min,-10):
			dac.setVolt(voltage)
			#time.sleep(.05)


def main():
	dac.setVolt(0)
	while True:
		Bright(0, 4000)
		#time.sleep(.5)
		Dim(4000, 0)
		#time.sleep(.5)


if __name__ == '__main__':
	main()