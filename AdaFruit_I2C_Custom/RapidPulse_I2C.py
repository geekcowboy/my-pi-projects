import sys
import time
from I2C_DAC import I2C_DAC

dac = I2C_DAC(0x60, True)

def Bright(min, max):
	if (min > max):
		dac.setVolt(0)
		print "Error detected.  Minimum voltage exceeds maximum."
		#time.sleep(3)
	else:
		voltage = min
		for voltage in range (min, max, 10):
			dac.setVolt(voltage)
			#time.sleep(.05)


def Dim(max, min):
	if (min > max):
		dac.setVolt(0)
		print "Error detected.  Minimum voltage exceeds maximum."
		#time.sleep(3)
	else:
		voltage = max
		for voltage in range (max,min,-10):
			dac.setVolt(voltage)
			#time.sleep(.05)


def main():
	while True:
		dac.setVolt(0)
		dac.setVolt(1000)
		dac.setVolt(2000)
		dac.setVolt(3000)
		dac.setVolt(4000)
		dac.setVolt(3000)
		dac.setVolt(2000)
		dac.setVolt(1000)
		dac.setVolt(0)


if __name__ == '__main__':
	main()