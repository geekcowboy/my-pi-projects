import sys

def main():
    ui = 0xFFFFABCD
    print hex(ui)
    ui = (ui & 0xFFFF)
    print hex(ui)

if __name__ == '__main__':
    main()