import sys
from Adafruit_MCP4725 import MCP4725
import time


def main():
	dac = MCP4725(0x60)
	while True:
		dac.setVoltage(4095)
		time.sleep(2)
		dac.setVoltage(0)
		time.sleep(2)

if __name__ == '__main__':
	main()