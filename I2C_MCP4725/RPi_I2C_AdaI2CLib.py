import sys
import time
from Adafruit_I2C import Adafruit_I2C

i2c = AdaFruit_I2C(self, 0x60, 1, False)

__REG_WRITEDAC         = 0x40
__REG_WRITEDACEEPROM   = 0x60
ONbytes                = [0x0F, 0xF]
OFFbytes               = [0x00, 0x00]

# Constructor
#def __init__(self, address=0x60, debug=False):
#	self.i2c = Adafruit_I2C(address)
#	self.address = address
#	self.debug = debug

def main():

	while True:
		self.i2c.writeList(self.__REG_WRITEDAC, ONbytes)
		time.sleep(2)
		self.i2c.writeList(self.__REG_WRITEDAC, OFFbytes)
		time.sleep(2)
	

if __name__ == '__main__':
	main()